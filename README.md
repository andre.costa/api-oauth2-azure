# Simple Golang API that connects with Azure AD through OAuth 2.0
This is a very simple API that can be used to retrieve user information from Azure AD through OAuth 2.0

## Endpoints

* `POST /auth` => creates a OAuth2 token with following JSON parameters:
   * `auth_type` => accepts currently only `oauth2` 
   * `auth_provider`=> accepts currently only `azure`
   * `auth_code` => when NOT provided, it shows OAuth2 URL; required to retrieve `AUTH_TOKEN`
* `GET /auth/callback` => shows authorization code with query parameters `state` and `code`; this endpoint is accessed automatically from Microsoft OAuth2 flow and should not be accessed directly
* `GET /me` => gets information about given bearer token user

## Assumptions

- [X] There is an app already registered at [Azure Portal](https://docs.microsoft.com/en-us/azure/active-directory/develop/quickstart-register-app) or [Developer App Registration Portal](https://apps.dev.microsoft.com)
- [X] Info from app registration is on the following environment variables:
    * `AZURE_AD_CLIENT_ID` (Client/Application ID)
    * `AZURE_AD_CLIENT_SECRET` (Client/Application Password)
    * `AZURE_AD_CLIENT_REDIRECT_URL` (Redirect/Callback URL defined for this Application)
    * `AZURE_AD_CLIENT_DOMAIN` (Azure AD Domain to be used at user e-mail definition)
       * e.g.: `gitlab.com`
- [X] Environment variable that sets the port to be used by localhost server at `LOCALHOST_PORT` in the format `:<port>`
    * e.g.: `LOCALHOST_PORT=":12345"`
- [X] jq is installed to filter JSON

## Build
* Cloning with `git clone git@gitlab.com:andre.costa/api-oauth2-azure.git`
* This project uses [dep](https://golang.github.io/dep/), so, first type `dep -v ensure` 
* Then build with `go build .`

## Execute
`.\api-oauth2-azure`

## cURL and Browse

### Get OAuth2 URL
```bash
curl -s -X POST -H 'Accept: application/json' -H 'Content-Type: application/json' \
--data '{"auth_type":"oauth2","auth_provider":"azure"}' http://localhost$LOCALHOST_PORT/auth \
| jq -r '.url'
```
### Browse to given URL
Just click on the given URL from previous cURL command and it should open default web browser and go to Microsoft OAuth2 Authorization page. You should enter your name and password and accept Read.User permissions. It will redirect to `AZURE_AD_CLIENT_REDIRECT_URL` defined and that will print out JSON with authorization code (`auth_code`).

### Set AUTH_CODE variable
Just copy contents from JSON field `auth_code` to environment variable AUTH_CODE.

### Get AUTH_TOKEN
```bash
AUTH_TOKEN=$(curl -s -X POST -H 'Accept: application/json' -H 'Content-Type: application/json' \
--data '{"auth_type":"oauth2","auth_provider":"azure","auth_code":"'"$AUTH_CODE"'"}' http://localhost$LOCALHOST_PORT/auth \
| jq -r '.access_token')
```
### Get your information
```bash
curl -H "Authorization: Bearer ${AUTH_TOKEN}"  http://localhost$LOCALHOST_PORT/me
```

### More information
Currently this API is stripping the full Azure AD given profile into a simpler one. If you want more information than that, please change function `func stripProfile(p *Person) *Profile` accordingly.