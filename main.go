package main

import (
	"context"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"

	"github.com/gofrs/uuid"
	gc "github.com/gorilla/context"
	"github.com/gorilla/mux"
	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/microsoft"
)

//AuthToken struct
type AuthToken struct {
	AuthType     string `json:"auth_type"`
	AuthProvider string `json:"auth_provider"`
	AuthCode     string `json:"auth_code"`
}

const (
	graphAPIResource string = "https://graph.microsoft.com/v1.0/"

	gravatarURI string = "https://www.gravatar.com/avatar/"

	headerContentType = "Content-Type"
	jsonContentType   = "application/json"
	headerCORS        = "Access-Control-Allow-Origin"
)

var (
	azureOauthConfig = &oauth2.Config{
		ClientID:     os.Getenv("AZURE_AD_CLIENT_ID"),
		ClientSecret: os.Getenv("AZURE_AD_CLIENT_SECRET"),
		RedirectURL:  os.Getenv("AZURE_AD_CLIENT_REDIRECT_URL"),
		Endpoint:     microsoft.AzureADEndpoint(""),
		Scopes:       []string{"User.Read"},
	}

	//SessionStore store instance
	SessionStore sessions.Store
)

//Person is type returned by Azure AD
type Person struct {
	BusinessPhones    []string
	DisplayName       string
	GivenName         string
	JobTitle          string
	Mail              string
	MobilePhone       string
	OfficeLocation    string
	PreferredLanguage string
	Surname           string
	UserPrincipalName string
	ID                string
}

//Profile is type to store user profile
type Profile struct {
	Username    string `json:"username"`
	FullName    string `json:"full_name"`
	ShortName   string `json:"short_name"`
	EMail       string `json:"email"`
	AvatarURL   string `json:"avatar_url"`
	GravatarURL string `json:"gravatar_url"`
}

func main() {

	// Configure storage method for session-wide information.
	cookieStore := sessions.NewCookieStore([]byte(securecookie.GenerateRandomKey(32)))
	cookieStore.Options = &sessions.Options{
		HttpOnly: true,
	}
	SessionStore = cookieStore

	router := mux.NewRouter()
	fmt.Println("Starting the API at http://locahost" + os.Getenv("LOCALHOST_PORT"))
	router.HandleFunc("/auth", handleCreateToken).Methods("POST")
	router.HandleFunc("/auth/callback", handleAzureCallback).Methods("GET")
	router.HandleFunc("/me", validateMiddleware(handleMe)).Methods("GET")
	log.Fatal(http.ListenAndServe(os.Getenv("LOCALHOST_PORT"), router))

}

func handleAzureCallback(w http.ResponseWriter, r *http.Request) {

	_, err := SessionStore.Get(r, r.FormValue("state"))
	if err != nil {
		respondWithError(w, http.StatusForbidden, "Invalid state parameter")
		return
	}

	code := r.FormValue("code")

	respond(w, http.StatusOK, map[string]string{"auth_code": code})

}

//CreateTokenEndpoint handler
func handleCreateToken(w http.ResponseWriter, r *http.Request) {
	var authToken AuthToken
	var token *oauth2.Token

	err := json.NewDecoder(r.Body).Decode(&authToken)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Cannot understand data given")
		return
	}

	if authToken.AuthType == "oauth2" {
		if authToken.AuthProvider == "azure" {
			if authToken.AuthCode == "" {
				sessionID := uuid.Must(uuid.NewV4()).String()
				_, err := SessionStore.New(r, sessionID)
				if err != nil {
					respondWithError(w, http.StatusInternalServerError, "Could not create oauth session")
					return
				}
				authURL := azureOauthConfig.AuthCodeURL(sessionID)
				message := "Auth Code is required. Get one at given URL"
				gc.Set(r, "session", sessionID)
				respondWithInfo(w, http.StatusNotAcceptable, message, authURL)
				return
			}
			token, err = azureOauthConfig.Exchange(context.Background(), authToken.AuthCode)
			if err != nil {
				respondWithError(w, http.StatusUnauthorized, "Failed to get authorization token")
				return
			}
		} else {
			respondWithError(w, http.StatusBadRequest, "Provider not accepted")
			return
		}
	} else {
		respondWithError(w, http.StatusBadRequest, "Authenticaton Type not accepted")
		return
	}
	respond(w, http.StatusCreated, token)
}

//ValidateMiddleware handler
func validateMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authorizationHeader := r.Header.Get("authorization")
		if authorizationHeader != "" {
			bearerToken := strings.Split(authorizationHeader, " ")
			if len(bearerToken) == 2 {
				token := &oauth2.Token{AccessToken: bearerToken[1]}
				if !token.Valid() {
					respondWithError(w, http.StatusForbidden, "Invalid authorization token")
				} else {
					gc.Set(r, "token", token)
					next(w, r)
				}
			}
		} else {
			respondWithError(w, http.StatusUnauthorized, "An authorization header is required")
		}
	})
}

func handleMe(w http.ResponseWriter, r *http.Request) {

	token := gc.Get(r, "token").(*oauth2.Token)

	profile, err := fetchProfile(context.Background(), token)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, "Error fetching user profile")
		return
	}
	respond(w, http.StatusOK, stripProfile(profile))
}

func respondWithError(w http.ResponseWriter, statusCode int, msg string) {
	respond(w, statusCode, map[string]string{"error": msg})
}

func respondWithInfo(w http.ResponseWriter, statusCode int, msg string, url string) {
	respond(w, statusCode, map[string]string{"info": msg, "url": url})
}

func respond(w http.ResponseWriter, statusCode int, payload interface{}) {

	w.Header().Set(headerCORS, "*")
	w.Header().Set(headerContentType, jsonContentType)
	w.WriteHeader(statusCode)
	enc := json.NewEncoder(w)
	enc.SetEscapeHTML(false)
	enc.Encode(payload)
}

// fetchProfile retrieves the Azure AD profile of the user associated with the
// provided OAuth token.
func fetchProfile(ctx context.Context, tok *oauth2.Token) (*Person, error) {
	client := oauth2.NewClient(ctx, azureOauthConfig.TokenSource(ctx, tok))

	response, err := client.Get(graphAPIResource + "me")
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()

	var person Person
	if err := json.NewDecoder(response.Body).Decode(&person); err != nil {
		return nil, err
	}

	return &person, nil
}

// stripProfile returns a subset of a Person.
func stripProfile(p *Person) *Profile {
	return &Profile{
		Username:    strings.TrimSuffix(p.Mail, "@"+os.Getenv("AZURE_AD_CLIENT_DOMAIN")),
		FullName:    p.DisplayName,
		ShortName:   p.GivenName,
		EMail:       p.Mail,
		AvatarURL:   graphAPIResource + fmt.Sprintf("users/%s/photo/$value", p.ID),
		GravatarURL: parseURI(emailToHash(p.Mail)).String(),
	}
}

func emailToHash(email string) string {
	hasher := md5.New()
	hasher.Write([]byte(email))
	return fmt.Sprintf("%v", hex.EncodeToString(hasher.Sum(nil)))
}

func parseURI(hash string) *url.URL {
	var URL *url.URL
	URL, err := url.Parse(gravatarURI)
	if err != nil {
		log.Fatal(err)
	}
	URL.Path += hash + ".jpg"
	return URL
}
